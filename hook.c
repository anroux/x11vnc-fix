#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <string.h>

#define _CONSTRUCTOR __attribute__((constructor))
#define _DESTRUCTOR __attribute__ ((destructor))

extern char *__progname;

struct hook_ctx {
        int (*SSL_CTX_set_cipher_list)(void *ssl, const char *str);
        int (*SSL_CTX_set_security_level)(void *ssl, int level);
};

static struct hook_ctx _ctx;

#define LOADORDIE(var, name) \
	do {\
		const char *err; \
		(var) = dlsym(RTLD_NEXT, (name)); \
		if ((err = dlerror()) != NULL) { \
			fprintf(stderr, "[%s] dlsym %s: %s\n", __progname, (name), err); \
			exit(EXIT_FAILURE); \
		} \
	} while(0)

void _CONSTRUCTOR hook_init(void) {
	unsetenv("LD_PRELOAD");

	dlerror();
	LOADORDIE(_ctx.SSL_CTX_set_cipher_list, "SSL_CTX_set_cipher_list");
	LOADORDIE(_ctx.SSL_CTX_set_security_level, "SSL_CTX_set_security_level");
}

void _DESTRUCTOR hook_fini(void) {
}

int SSL_CTX_set_cipher_list(void *ssl, const char *str) {
	if (strcmp(str, "ADH:@STRENGTH") == 0) {
		_ctx.SSL_CTX_set_security_level(ssl, 0);
		return _ctx.SSL_CTX_set_cipher_list(ssl, "ADH:@SECLEVEL=0");
	}

	return _ctx.SSL_CTX_set_cipher_list(ssl, str);
}
