CFLAGS+=-Wall -Werror -g
LDFLAGS+=-lc -ldl
RM?=rm -f

.PHONY:clean

all: hook.so.1

hook.so.1: hook.c
	$(CC) $(CFLAGS) -fPIC -shared -o $@ $< $(LDFLAGS)

clean: 
	$(RM) hook.so.1
